#!/bin/bash
export PDCI_TAG_LATEST=${PDCI_TAG_LATEST:-latest}
export PDCI_TAG_TESTING=${PDCI_TAG_TESTING:-testing}
export PDCI_ENV_PRODUCTION=${PDCI_ENV_PRODUCTION:-production}
export PDCI_ENV_TESTING=${PDCI_ENV_TESTING:-testing}
export PDCI_DOMAIN_PORT_REGISTRY=${PDCI_DOMAIN_PORT_REGISTRY:-${pdci_domain_port_registry}}
export PDCI_DOCKER_HOST_DSV=${PDCI_DOCKER_HOST_DSV:-${pdci_docker_host_dsv}}
export DOCKER_HOST=${PDCI_DOCKER_HOST_DSV}
cp -vf ./test/goss/goss.production.yaml ./goss.yaml
docker build -f Dockerfile --target  ${PDCI_ENV_PRODUCTION} -t pdci/base_co7_httpd_php-5.6:${PDCI_TAG_LATEST} . #--no-cache
dgoss run -e  GOSS_OPTS="--color --format junit"  -p 8547:80   pdci/base_co7_httpd_php-5.6:${PDCI_TAG_LATEST}
rm -vf ./goss.yaml
#cp -f ./test/goss/goss.testing.yaml ./goss.yaml
#docker  build -f Dockerfile --target  ${PDCI_ENV_TESTING} -t pdci/co7_httpd_laravel:${PDCI_TAG_TESTING} . #--no-cache
#dgoss run  -p 8547:80  pdci/co7_httpd_laravel:${PDCI_TAG_TESTING}
#rm -f ./goss.yaml